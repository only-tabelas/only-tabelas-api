package config

import (
	"database/sql"
	"log"

	// nescessário para conexao com o banco
	_ "github.com/go-sql-driver/mysql"
)

// GetConexao Retorna a conexão com o Banco de dados
func GetConexao() *sql.DB {
	dbDados := getKeyEnv("BD_USER") + ":" + getKeyEnv("BD_PASSWORD") + "@/" + getKeyEnv("BD_NAME")
	db, err := sql.Open("mysql", dbDados)
	if err != nil {
		log.Fatal("Error ao tentar conectar ao banco\n", err)
	}
	return db
}

// CloseConection Fecha a conexão com o banco
func CloseConection(db *sql.DB) {
	db.Close()
}

// Exec executa um comando SQL
func Exec(db *sql.DB, sql string) sql.Result {
	result, err := db.Exec(sql)
	if err != nil {
		panic(err)
	}
	return result
}
