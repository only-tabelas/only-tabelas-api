package config

import (
	"log"
	"net/http"
)

// UpServer Sobe o Server
func UpServer() {
	port := ":" + getKeyEnv("SERVER_PORT")
	log.Fatal(http.ListenAndServe(port, nil))
}
