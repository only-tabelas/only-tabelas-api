package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

func getKeyEnv(key string) string {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	value := os.Getenv(key)
	if len(value) == 0 {
		log.Fatal("Chave " + key + " not exist in .env file")
	}
	return value
}
