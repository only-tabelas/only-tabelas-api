package main

import (
	"fmt"
	"net/http"

	"./config"
)

func foo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hello World</h1>")
}

func main() {
	db := config.GetConexao()
	fmt.Println("Banco Conectado")
	defer config.CloseConection(db)

	http.HandleFunc("/", foo)

	config.UpServer()
}
